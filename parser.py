"""
Класс для парсинга патентов

(с) Алейников А.А.

14.05.2022
"""
import sys

from bs4 import BeautifulSoup as bs

from database_connector import DatabaseClient
from dictionary import *
import os
import re
import json

"""
Метод добавляет кавычки в начало и конец строки 

Аргументы:
str - строка для обработки
"""


def reformatString(s: str):
    return "'" + s + "'"


class PatentParser:

    # Иницализация счетчиков и клиента БД
    def __init__(self):
        self.client = DatabaseClient().createClient()
        self.count_patent = 1
        self.count_applicants = 1
        self.count_patent_applicant = 1
        self.count_assignees = 2
        self.count_classes = 1
        self.count_patent_class = 1
        self.count_citations = 1
        self.count_patent_citation = 1

        self.count_docs = 0
        self.count_records_doc = 0

    """
    Метод обрабатывает заявителей и записывает в базу данных

    Аргументы:
    patent - текст патента
    """

    def setApplicants(self, patent):
        client = self.client
        try:
            applicants = patent.find("us-applicants")
            applicants = applicants.findAll("us-applicant")
            # Цикл по заявителям
            for applicant in applicants:
                # Создание имени
                first_name = applicant.find("first-name")
                last_name = applicant.find("last-name")
                full_name = first_name.contents[0] + " " + last_name.contents[0]
                full_name = full_name.replace("\'", "\\\'")
                full_name = reformatString(full_name)

                # Проверка есть ли это имя в базе
                query = "select count() from (select * from %s where fullname = %s)" % (
                    applicants_table, full_name)
                c = client.execute(query)[0][0]
                found = True if c == 1 else False

                # Если заявитель уже есть в БД, поиск его id и запись в таблицу патент-заявитель
                if (found):
                    query = "select id from %s where fullname = %s" % (
                        applicants_table, full_name)
                    applicant_id = client.execute(query)[0][0]
                    query = "insert into %s values (%d, %d, %d)" % (
                        patent_applicant_table, self.count_patent_applicant, self.count_patent,
                        applicant_id)
                    client.execute(query)
                    self.count_patent_applicant += 1
                # если заявителя нет в БД, запись в обе таблицы
                else:
                    query = "insert into %s values (%d, %s)" % (
                        applicants_table, self.count_applicants, full_name)
                    client.execute(query)
                    self.count_applicants += 1

                    query = "select id from %s where fullname = %s" % (
                        applicants_table, full_name)
                    applicant_id = client.execute(query)[0][0]
                    query = "insert into %s values (%d, %d, %d)" % (
                        patent_applicant_table, self.count_patent_applicant, self.count_patent,
                        applicant_id)
                    client.execute(query)
                    self.count_patent_applicant += 1
        except Exception as error:
            print("Ошибка при добавлении заявителя" + str(error))

    """
    Метод обрабатывает владельцев патента и записывает в базу данных

    Аргументы:
    patent - текст патента
    """

    def setAssignees(self, patent):
        client = self.client
        try:
            assignees = patent.find("assignees")
            orgname = assignees.find("orgname")
            orgname = orgname.contents[0]
            orgname = orgname.replace("\'", "\\\'")
            orgname = reformatString(orgname)

            query = "select id from %s where company_name == %s" % (assignee_table, orgname)
            c = client.execute(query)
            if (len(c) == 0):
                query = "insert into %s values (%d, %s)" % (
                    assignee_table, self.count_assignees, orgname)
                client.execute(query)
                org_id = self.count_assignees
                self.count_assignees += 1
            else:
                org_id = c[0][0]

        except Exception as error:
            org_id = 1
        return org_id

    """
    Метод обрабатывает классы патента и записывает в базу данных

    Аргументы:
    classifications - лист классификаций
    """

    def setClassification(self, classifications):
        client = self.client
        for classification in classifications:
            try:
                # Создание патентного класса
                section = classification.find("section")
                section = section.contents[0]
                cl = classification.find("class")
                cl = cl.contents[0]
                subclass = classification.find("subclass")
                subclass = subclass.contents[0]
                main_group = classification.find("main-group")
                main_group = main_group.contents[0]
                subgroup = classification.find("subgroup")
                subgroup = subgroup.contents[0]
                final_class = section + cl + subclass + main_group + "/" + subgroup
                final_class = reformatString(final_class)

                # Проверка записан ли класс в базу
                query = "select id from %s where classification == %s" % (
                    class_table, final_class)
                c = client.execute(query)
                if (len(c) == 0):
                    query = "insert into %s values (%d, %s)" % (
                        class_table, self.count_classes, final_class)
                    client.execute(query)
                    class_id = self.count_classes
                    self.count_classes += 1
                else:
                    class_id = c[0][0]

                query = "insert into %s values (%d, %d, %d)" % (
                    patent_class_table, self.count_patent_class, self.count_patent, class_id)
                client.execute(query)
                self.count_patent_class += 1
            except Exception as error:
                print("Ошибка при добавлении класса" + str(error))

    """
    Метод обрабатывает цитаты патента и записывает в базу данных

    Аргументы:
    patent - текст патента
    """

    def setCitation(self, patent):
        client = self.client
        citations = patent.find("us-references-cited")
        if citations != None:
            citations = citations.findAll("patcit")
            for cit in citations:
                try:
                    # Создание патентного класса
                    country = cit.find("country")
                    country = country.contents[0]
                    nubmer = cit.find("doc-number")
                    nubmer = nubmer.contents[0]
                    kind = cit.find("kind")
                    kind = kind.contents[0]
                    final_num = country + nubmer + kind
                    final_num = reformatString(final_num)

                    # Проверка записан ли класс в базу
                    query = "select id from %s where citation_number == %s" % (
                        citations_table, final_num)
                    c = client.execute(query)
                    if (len(c) == 0):
                        query = "insert into %s values (%d, %s)" % (
                            citations_table, self.count_citations, final_num)
                        client.execute(query)
                        citation_id = self.count_citations
                        self.count_citations += 1
                    else:
                        citation_id = c[0][0]

                    query = "insert into %s values (%d, %d, %d)" % (
                        patent_citation_table, self.count_patent_citation, self.count_patent, citation_id)
                    client.execute(query)
                    self.count_patent_citation += 1
                except Exception as error:
                    print("Ошибка при добавлении цитаты" + str(error))

    """
    Метод обрабатывает описание патента и записывает в базу данных

    Аргументы:
    client - клиент базы данных
    description - текст описания
    """

    def setDescription(self, client, description):
        description = str(description)
        description = description.replace("\"", "\\\"")
        description = description.replace("\'", "\\\'")
        description = re.sub(r'\<figref.*?figref\>', '', description)
        description = re.sub(r'\<heading.*?heading\>', '', description)
        description = re.sub(r'\<[^>]*\>', '', description)
        description = reformatString(description)
        self.client.execute("drop table if exists desc_tmp")
        self.client.execute(
            "create table desc_tmp (id int, description String) "
            "ENGINE=HDFS('hdfs://namenode:9000/user/root/description/d%d.csv', 'CSV');" % (
                self.count_patent))
        query = "insert into desc_tmp values (%d, %s)" % (self.count_patent, description)
        client.execute(query)
        self.client.execute("drop table if exists desc_tmp")

    """
    Метод обрабатывает формулу патента и записывает в базу данных

    Аргументы:
    client - клиент базы данных
    claims - текст формулы патента
    """

    def setClaims(self, client, claims):
        claims = str(claims)
        claims = claims.replace("\"", "\\\"")
        claims = claims.replace("\'", "\\\'")
        claims = reformatString(claims)
        self.client.execute("drop table if exists claims_tmp")
        self.client.execute(
            "create table claims_tmp (id int, claim String) "
            "ENGINE=HDFS('hdfs://namenode:9000/user/root/claim/c%d.csv', 'CSV');" % (
                self.count_patent))
        query = "insert into claims_tmp values (%d, %s)" % (self.count_patent, claims)
        client.execute(query)
        self.client.execute("drop table if exists claims_tmp")

    """
    Метод проверяет, подходит ли патент под категории из конфига

    Аргументы:
    categories - категории из конфига
    classifications - лист классификаций патента
    max_category_count - максимальное количество патентов одной категории
    """

    def checkCategories(self, categories, classifications, max_category_count):
        if (categories != None):
            if classifications.__len__() == 1:
                for classification in classifications:
                    # Создание патентного класса
                    section = classification.find("section")
                    section = section.contents[0]
                    cl = classification.find("class")
                    cl = cl.contents[0]
                    subclass = classification.find("subclass")
                    subclass = subclass.contents[0]
                    main_group = classification.find("main-group")
                    main_group = main_group.contents[0]
                    subgroup = classification.find("subgroup")
                    subgroup = subgroup.contents[0]
                    final_class = section + cl + subclass + main_group + "/" + subgroup
                    for category in categories:
                        if final_class.startswith(category):
                            if max_category_count is None:
                                return True
                            else:
                                count = self.client.execute(""" select count(DISTINCT invention_title) from %s
                                                            inner join %s pc on %s.id = pc.patent_id
                                                            left join %s c on pc.classification_id = c.id
                                                        where startsWith(c.classification, '%s')
                                                    """ % (
                                    patents_table, patent_class_table, patents_table, class_table, category))
                                count = count[0][0]
                                if count < max_category_count:
                                    return True

                return False
            else:
                return False
        else:
            return True

    """
    Метод читает параметры парсинга из файла конфигурации
    """

    def readConfig(self):
        with open(os.path.join(sys.path[0], 'config'), 'r') as conf:
            categories = None
            max_category_count = None
            content = conf.readlines()
            content = "".join(content)
            content = content.replace(" ", "")
            content = content.replace("\n", "")
            config_list = json.loads(content)
            if 'categories' in config_list:
                categories = config_list["categories"].split(",")

            if 'max_category_count' in config_list:
                max_category_count = config_list["max_category_count"]
        return categories, max_category_count

    """
    Метод для запуска парсинга
    """

    # основной парсер
    def parse(self):
        try:
            categories, max_category_count = self.readConfig()
        except Exception as error:
            print("Файл конфигурации не найден")
            return


        for filename in os.listdir(uspto_dir):
            with open(os.path.join(uspto_dir, filename), 'r') as file:
                # Создание клиента базы данных
                client = self.client

                self.count_records_doc = 0
                self.count_docs += 1

                # Очистка файла от мусора
                content = file.readlines()
                content = "".join(content)
                content = content.replace("\n", "")

                # Разбиение файла по патентам
                patent_list = content.split("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")

                # Цикл по патентам
                for content in patent_list:
                    self.count_records_doc += 1
                    # Скип пустых патентов
                    if len(content) != 0:
                        bs_content = bs(content, "lxml")
                        patent = bs_content.find("us-patent-grant")

                        print("Файл: " + str(self.count_docs) + " Процент обработки: " + str(
                            round((self.count_records_doc / len(patent_list) * 100), 3)) + " %")
                        # если есть тег us-patent-grant
                        if patent != None:
                            classifications = patent.find("classifications-ipcr")
                            # если у патента есть международная классификация
                            if classifications != None:
                                classifications = classifications.findAll("classification-ipcr")
                                # если есть классификауия
                                if classifications != None:
                                    classification = classifications[0]
                                    section = classification.find("section")
                                    section = section.contents[0]
                                    # если патентный класс один из необходимых
                                    if (section == "I" or section == "F" or section == "G" or section == "H"):
                                        if self.checkCategories(categories, classifications, max_category_count):

                                            # Название патента
                                            inv_title = patent.find("invention-title")
                                            inv_title = inv_title.contents[0]

                                            # Дата патента и страна
                                            pub_ref = patent.find("publication-reference")
                                            country = pub_ref.find("country")
                                            doc_num = pub_ref.find("doc-number")
                                            date = pub_ref.find("date")
                                            kind = pub_ref.find("kind")

                                            # Аннтоция
                                            abstract = patent.find("abstract")
                                            abstract = abstract.find("p")
                                            abstract = abstract.text
                                            abstract = abstract.replace("\"", "\\\"")
                                            abstract = abstract.replace("\'", "\\\'")
                                            abstract = reformatString(abstract)

                                            # Описание
                                            description = patent.find("description")
                                            claims = patent.find("claims")

                                            country = country.contents[0]
                                            doc_num = doc_num.contents[0]
                                            date = str(date.contents[0])
                                            kind = kind.contents[0]
                                            final_num = str(country + doc_num + kind)

                                            # Название компании владельца патента
                                            org_id = self.setAssignees(patent)

                                            # Запись данных в таблицу патентов
                                            try:
                                                # Добавление кавычек
                                                date = reformatString(date)
                                                final_num = reformatString(final_num)
                                                inv_title = reformatString(inv_title)

                                                query = "insert into %s values (%d, %s, %s, %s, %s, %d)" % (
                                                    patents_table, self.count_patent, inv_title, final_num, date,
                                                    abstract, org_id)
                                                client.execute(query)
                                            except Exception as error:
                                                print("Ошибка записи патента в БД:" + str(error))
                                                continue
                                            try:
                                                # Обработка описания
                                                self.setDescription(client, description)
                                            except Exception as error:
                                                print("Ошибка записи описания в БД:" + str(error))

                                            try:
                                                # Обработка claims
                                                claims = claims.text
                                                self.setClaims(client, claims)
                                            except Exception as error:
                                                print("Ошибка записи claim в БД:" + str(error))

                                            # Обработка заявителей
                                            self.setApplicants(patent)

                                            # Обработка классификаций
                                            self.setClassification(classifications)

                                            # Обработка цитат
                                            self.setCitation(patent)

                                            # увеличение счетчика патентов
                                            self.count_patent += 1

                content = None
        client.disconnect()
