#пути
uspto_dir = "/home/andrew/uspto"
# Названия таблиц базы данных
database_name = "test1"
patents_table = database_name + ".patents"
patent_applicant_table = database_name + ".patent_applicant"
applicants_table = database_name + ".applicants"
assignee_table = database_name + ".assignees"
class_table = database_name + ".classifications"
patent_class_table = database_name + ".patent_classification"
citations_table = database_name + ".citations"
patent_citation_table = database_name + ".patent_citation"
description_table = database_name + ".hdfs_description"
claim_table = database_name + ".hdfs_claim"
